# "THE BEER-WARE LICENSE" (Revision 42):
# <robin.hahling@gw-computing.net> wrote this file. As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day, and
# you think this stuff is worth it, you can buy me a beer in return.
# Rigel Kent

require 'net/http'
    
class String
  def remove_lines_begin(i)
    self.lines.to_a[i.to_i..-1].join
  end
  def remove_lines_end(i)
    self.lines.to_a[i.to_i..1].join
  end
end

module Jekyll
  # Remotely fetch a file and include only lines given
  class RemoteLinesTag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super
      
      @params = split_params(text)
      @url = @params[0]
      @lines_begin = @params[1]
      @lines_end = @params[2]

      check_protocol(@url)
      uri = URI(@url)

      res = Net::HTTP.get_response(uri)
      fail 'resource unavailable' unless res.is_a?(Net::HTTPSuccess)

      @content = res.body.force_encoding("UTF-8")
    end

    def render(_context)
      @content.remove_lines_begin(@lines_begin)
    end

    private

    def check_protocol(text)
      error_message = "remote_lines: invalid URI given #{text}"
      fail error_message unless text =~ URI.regexp(%w(http https ftp ftps))
    end

    def split_params(params)
      params.split("|")
    end
  end
end

Liquid::Template.register_tag('remote_lines', Jekyll::RemoteLinesTag)

