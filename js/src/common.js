import 'jquery';
import 'bootstrap/js/dist/dropdown';
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/tab';
//import React from 'react';
//import ReactDOM from 'react-dom';
import { fillLanguageDropdown } from './lib/production';

//import Search from './lib/Search';
//import { checkServiceStatus } from './lib/serviceStatus';

//checkServiceStatus();

//ReactDOM.render(<Search />, document.getElementById('search'));

if (process.env.NODE_ENV === 'production') {
  fillLanguageDropdown();
}
