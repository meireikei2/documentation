---
id: devdocs_architecture
layout: guide
timer: false
---

{% include vars.html %}

PeerTube is more than just a web page. Simplifying things a lot, we can
see it as two parts: a client application that executes in the browser of
each visitor (and that can be replace entirely with a new client of your
choice, of course), and a server part that resides on the machine of the
instance's system administrator.

As is common among modern applications, it is in reality made of several
more components than just a "client" and "server", each of them fulfilling
a specific mission:

- a modern database engine (PostgreSQL) to store long-term metadata
- a reverse proxy (we officially support Nginx but nothing prevents using others)
  to handle certificates, and directly serve static assets
- a key-value store (Redis) to help application caching and task queueing
- a REST API server providing the actual logic, data streaming and embeding an in-memory BitTorrent tracker
- a web client _Single Page Application_ that consumes the REST API
- a user that interacts with the web client of his choice

{% digraph PeerTube Example Setup %}
graph [label="PeerTube Example Setup"]

reverse [label = "Reverse Proxy"];
web_client [label = "Web Clients"];
video_storage [label = "video storage"];
task_queue [label = "Task Queue\n(Bull)"];
ffmpeg [label = "local FFmpeg worker"];
peertube [href="#the-rest-api-server", label = "PeerTube API server\n(Express)"];
static [label = "static assets"];

reverse -> {
video_storage
peertube
static
web_client
peertube
};
web_client -> reverse [xlabel = "HTTP"];
web_client -> web_client [xlabel = "P2P"];

peertube -> task_queue;
peertube -> peertube [href="#the-database", label = "Redis/PostgreSQL"];
task_queue -> ffmpeg;
ffmpeg -> task_queue;

ffmpeg -> video_storage [style = dotted, label = "video resolutions"];
ffmpeg -> static [style = dotted, label = "thumbnails\ntorrents"];
{% enddigraph %}

Let's detail each component:

#### The user

PeerTube users can interact with your instance using:

- The official web interface
- Third-party apps (other clients using the REST API)

#### The web interface

This refers to PeerTube's official web interface, which is a Single Page application
written in Angular. This application will interact with PeerTube's API to retrieve
or send data. Of course any alternative client interface can be used so long as it is
compatible with the API, and PeerTube can be started without the official web interface
to let you serve your client instead.

#### The reverse proxy

PeerTube's API server should never be exposed directly to the internet, as we require
a reverse proxy (we provide support for Nginx) for performance and security. The reverse proxy
will receive client HTTP requests, and:

- Proxy them to the API server
- Serve requested static files (Video files, stylesheets, javascript, fonts...)

#### The REST API server

The API server is the central piece of PeerTube. This component is responsible
for answering and processing user requests, manipulate data from the database, send long-running
tasks to the local worker, etc.

It's an [Express](https://expressjs.com/) application.

#### The database

Most of the data such as user accounts, video metadata, comments or channels are stored
in a PostgreSQL database.

#### The cache/job queue

Fetching data from the database is sometimes slow or resource hungry. To reduce
the load, Redis is used as a cache for route data meant to be stored temporarily.

It is also a message queue that will deliver tasks to the local worker. Indeed PeerTube
uses the [Bull queue](https://github.com/OptimalBits/bull) which doesn't support remote
workers yet.

## Vocabulary <a class="toc" id="toc-lexicon" href="#toc-lexicon"></a>

Fediverse
: several servers following one another, several users
following each other. Designates federated communities in general.

Vidiverse
: same as Fediverse, but federating videos specifically.

Instance
: a server which runs PeerTube in the fediverse.

Origin instance
: the instance on which the video was uploaded and which
is seeding (through the WebSeed protocol) the video.

Cache instance
: an instance that decided to make available a WebSeed
of its own for a video originating from another instance. It sends a [`CacheFile`
activity]({{url_base}}/devdocs/federation.html#toc-cachefile)
to notify the origin instance, which will then update its list of
WebSeeds for the video.

Following
: the action of a PeerTube instance which will follow another
instance (subscribe to its videos). You can read more about Follows in the admin doc,
under [following servers]({{url_base}}/docs/following-servers.html).

## Base <a class="toc" id="toc-base" href="#toc-base"></a>

#### What is an instance and how does it work

- An instance has a websocket tracker which is responsible for all videos
  uploaded by its users.
- An instance has an administrator that can follow other instances.
- An instance can be configured to follow back automatically.
- An instance can blacklist other instances (only used in "follow back"
  mode).
- An instance cannot choose which other instances follow it, but it can
  decide to **reject all** followers.
- After having uploaded a video, the instance seeds it (WebSeed protocol).
- If a user wants to watch a video, they ask its instance the magnet URI and
  the frontend adds the torrent (with WebTorrent), creates the HTML5 video
  player and streams the file into it.
- A user watching a video seeds it too (BitTorrent). Thus another user who is
  watching the same video can get the data from the origin server and other
  users watching it.

#### Communications between instances

![Sharing videos metadata to build an index of more than just local videos](/assets/architecture/decentralized.png){:class="img-fluid mx-auto d-block"}

- All the communications between the instances are signed with [JSON Linked Data
  Signatures](https://w3c-dvcg.github.io/ld-signatures/) with the private key
  of the account that authored the action.
- We use the [ActivityPub](https://www.w3.org/TR/activitypub/) protocol (only
  server-server for now) as the high-level federation protocol. Object models can be found in
  [shared/models/activitypub
  directory](https://github.com/Chocobozzz/PeerTube/tree/develop/shared/models/activitypub).
- [ActivityStreams](https://www.w3.org/TR/activitystreams-core/) and
  [ActivityStreams](https://www.w3.org/TR/activitystreams-vocabulary/) vocabulary
  as the mean to structure messages.
- All the requests are retried several times if they fail.
- Actor requests are authentified via [HTTP Signatures](https://datatracker.ietf.org/doc/draft-cavage-http-signatures/) as the secondary mean
  to authenticate messages to keep compatible security-wise with textual instances
  like Mastodon which rely on it.

#### Redundancy between instances

A PeerTube instance can cache other PeerTube videos to improve bandwidth of popular videos
or small instances.

##### How it works

The instance administrator can choose between multiple redundancy strategies (cache trending videos or recently uploaded videos etc), set their maximum size and the minimum duplication lifetime.
Then, they choose the instances they want to cache in `Manage follows -> Following` admin table.

Videos are kept in the cache for at least `min_lifetime`, and then evicted when the cache is full.

When PeerTube chooses a video to duplicate, it imports all the resolution files (to avoid consistency issues) using their magnet URI and put them in the `storage.videos` directory.
Then it sends a `Create -> CacheFile` ActivityPub message to other federated instances. This new instance is injected as [WebSeed](https://github.com/Chocobozzz/PeerTube/blob/develop/FAQ.md#what-is-webseed) in the magnet URI by instances that received this ActivityPub message.

![Cache servers](/assets/architecture/redundancy.png){:class="img-fluid mx-auto d-block"}

##### Stats

See the `/api/v1/server/stats` endpoint. For example:

```json
{
  ...
  "videosRedundancy": [
    {
      "totalUsed": 0,
      "totalVideos": 0,
      "totalVideoFiles": 0,
      "strategy": "most-views",
      "totalSize": 104857600
    },
    {
      "totalUsed": 0,
      "totalVideos": 0,
      "totalVideoFiles": 0,
      "strategy": "trending",
      "totalSize": 104857600
    },
    {
      "totalUsed": 0,
      "totalVideos": 0,
      "totalVideoFiles": 0,
      "strategy": "recently-added",
      "totalSize": 104857600
    }
  ]
}
```
