---
id: devdocs_index
layout: guide
timer: false
---

## References

- [How to Contribute](https://github.com/Chocobozzz/PeerTube/blob/develop/.github/CONTRIBUTING.md#develop) (general insight on how to setup a development workflow)
- [Troubleshooting](troubleshooting.html)

## Tutorials

- [How to setup a development environment on a VPS](vps.html)

## Background

Adding a little context and discussing the technologies that power PeerTube,
but also explaining why they are used and how.

- [Architecture: Base](architecture.html)
- [Architecture: Federation](federation.html)
