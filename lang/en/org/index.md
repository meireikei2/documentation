---
id: organization
guide: peertube_organization
layout: guide
timer: false
---

{% include vars.html %}

<p class="lead">
  Rules, guidelines, and documentation on PeerTube contribution, processes, and
  community.
</p>

PeerTube is a community run project sponsored by Framasoft. Anyone can get involved and contribute to PeerTube (or otherwise fork it but wait we're nice!), and we're committed to creating an open and inclusive community for everyone.

Every member of the community that wishes to contribute either through code,
documentation, support, or any other form of contribution must read and follow
our [Code of Conduct]({{url_base}}/org/code-of-conduct).
