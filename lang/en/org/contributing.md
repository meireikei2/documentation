---
id: contributing
guide: peertube_organization
layout: guide
timer: false
---

{% include vars.html %}

Contributions are always welcome, no matter how large or small. Before contributing,
please read the [code of conduct]({{url_base}}/org/code-of-conduct). While this page is an introduction, please refer to [the contributing page on the repository](https://github.com/Chocobozzz/PeerTube/blob/develop/.github/CONTRIBUTING.md#welcome-to-the-contributing-guide-for-peertube).

## Find things to work on <a class="toc" id="toc-find-things-to-work-on" href="#toc-find-things-to-work-on"></a>

These are the main categories that you can work on. We further mark issues with a `high-priority` tag or a `good first issue` tag to indicate their importance to the project and subjective level of easiness to get started on respectively. If you don't see the `enhancement` tag or you see any of the `to reproduce` or `discussion` tags, it may not be wise to start working on these issues.

Here are a few quick links to get you started:

- [Good first features](https://github.com/Chocobozzz/PeerTube/issues?q=is%3Aissue+is%3Aopen+sort%3Aupdated-desc+label%3A%22good+first+issue%22)

## Building <a class="toc" id="toc-building" href="#toc-building"></a>

Please refer to the [develop guide](https://github.com/Chocobozzz/PeerTube/blob/develop/.github/CONTRIBUTING.md#develop).

## Pull Requests <a class="toc" id="toc-pull-requests" href="#toc-pull-requests"></a>

We actively welcome your pull requests.

1.  Fork the repo and create your branch from `develop`.
2.  If you've added code that should be tested, add tests.
3.  If you've changed APIs, update the documentation.
4.  Ensure the test suite passes.
5.  Make sure your code lints.

## License <a class="toc" id="toc-license" href="#toc-license"></a>

By contributing to PeerTube, you agree that your contributions will be licensed
under its [AGPLv3.0 license](https://github.com/Chocobozzz/PeerTube/blob/master/LICENSE).
