This is the recommended way to install PeerTube. It ensures we can provide you
with the best support, as community packages are potentially less maintained
and are harder to debug for us.

The documentation for the **[production install method is in the main repository](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md)**,
to always be in sync.
