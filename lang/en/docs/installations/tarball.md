{% comment %}

#### Installation Script

One of the easiest ways to install PeerTube on macOS and generic Unix environments
is via our shell script. You can install PeerTube by running the following code in
your terminal:

<div class="install-only-stable" markdown="1">
```sh
curl -o- -L https://pkg.rigelk.eu/install.sh | vipe | bash
```
</div>
<div class="install-only-beta" markdown="1">
```sh
curl -o- -L https://pkg.rigelk.eu/install.sh | vipe | bash -s -- --beta
```
</div>
<div class="install-only-rc" markdown="1">
```sh
curl -o- -L https://pkg.rigelk.eu/install.sh | vipe | bash -s -- --rc
```
</div>
<div class="install-only-nightly" markdown="1">
```sh
curl -o- -L https://pkg.rigelk.eu/install.sh | vipe | bash -s -- --nightly
```
</div>

The installation process includes verifying a GPG signature.
[View the source](https://git.rigelk.eu/rigelk/peertube.pkg.rigelk.eu/src/branch/master/install.sh)

<div class="install-only-stable" markdown="1">
You can also specify a version by running the following code in your terminal:

```sh
curl -o- -L https://pkg.rigelk.eu/install.sh | vipe | bash -s -- --version [version]
```

See [the releases](https://github.com/Chocobozzz/PeerTube/releases) for possible versions.

</div>

{% endcomment %}

#### Manual install via tarball

You can install a release of PeerTube by [downloading its tarball](https://github.com/Chocobozzz/PeerTube/releases/latest).

<div class="install-only-stable" markdown="1">
```sh
cd /var/www/
curl -s https://api.github.com/repos/Chocobozzz/PeerTube/releases/latest \
 | grep browser_download_url \
 | grep tar.xz \
 | cut -d '"' -f 4 \
 | wget -qi -
tar -xJf <archive_name>
# PeerTube is now in /var/www/peertube-[version]/
```
</div>
<div class="install-only-beta" markdown="1">
```sh
cd /var/www
curl -s https://api.github.com/repos/Chocobozzz/PeerTube/releases/latest \
 | grep browser_download_url \
 | grep tar.xz \
 | cut -d '"' -f 4 \
 | wget -qi -
tar -xJf <archive_name>
# PeerTube is now in /var/www/peertube-[version]/
```
</div>
<div class="install-only-nightly" markdown="1">
```sh
cd /var/www
curl -L https://api.github.com/repos/Chocobozzz/PeerTube/tarball/develop
tar -xJf <archive_name>
# PeerTube is now in /var/www/peertube-[version]/
```
</div>

Before extracting PeerTube, it is recommended that you verify the tarball using GPG:

```sh
curl -s 'https://api.github.com/users/Chocobozzz/gpg_keys' \
 | python3 -c "import sys, json; print(json.load(sys.stdin)[0]['raw_key'])" \
 | gpg --import
```

Now verify it:

```sh
gpg --verify <archive_name>
# Look for "Good signature from 'Chocobozzz'" in the output
```

Now that you're in possession of the latest release, you can follow [the reference production guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md).
