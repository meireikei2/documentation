<div class="install-only-beta" markdown="1">
On CentOS, Fedora and RHEL, you can install PeerTube via a __community package__ made by [daftaupe](https://copr.fedorainfracloud.org/coprs/daftaupe/peertube/) on COPR.

```sh
dnf copr enable daftaupe/peertube
```

### Prerequisites

You will need PostgreSQL, Node.JS and FFMpeg :

* Fedora you need RPM Fusion repository enabled
  ```sh
  sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
  ```
* CentOS you will need EPEL and the unofficial EPEL-multimedia repositories enabled
  ```sh
  cd /etc/yum.repos.d && curl -O https://negativo17.org/repos/epel-multimedia.repo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm https://negativo17.org/repos/epel-multimedia.repo
  ```

**Setup the database**

```sh
su - postgres
initdb
createuser peertube -W
createdb -O peertube peertube_prod
echo "host peertube peertube 127.0.0.1/32 md5" >> data/pg_hba.conf
```

```sh
systemctl reload postgresql
```

**Start the services**

```sh
systemctl start redis
```

**Edit the configuration to fit your needs**

```sh
vim /etc/peertube/production.yaml
```

**Start PeerTube and get the initial root / password**

```sh
systemctl start peertube && journalctl -f -u peertube
```

</div>

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no RPM packages available for RC or nightly builds of PeerTube. Please use the tarball:
{% include_relative installations/tarball.md %}
</div>
