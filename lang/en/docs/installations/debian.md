<div class="alert alert-warning" role="alert">
  <strong>Warning</strong>: the debian package is left for you to build but we do not build it ourselves anymore. We thus
  recommend you to install the package via the <a href="https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md">reference production guide</a>.
</div>

<div class="install-only-beta" markdown="1">
On Debian or Ubuntu Linux, you can install Yarn, Node and PeerTube in one shot via this __community maintained__ debian package definition. It is left for you to build at [framagit.org/rigelk/package-debian-peertube](https://framagit.org/rigelk/package-debian-peertube).

Afterwards you still have to [configure the database](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#database) and credentials to it in the configuration file of PeerTube in `/etc/peertube/production.yaml`.

</div>

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no Debian packages available for RC or nightly builds of PeerTube. Please use the [production guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md).
</div>
