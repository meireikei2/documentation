---
id: docs_cli_index
guide: docs_cli
layout: guide
---

{% include vars.html %}

PeerTube comes with a few utilities that can be run independently from the instance and whether it is setup or not.

## Prerequisites <a class="toc" id="toc-prerequisites" href="#toc-prerequisites"></a>

You need at least 512MB of RAM to run the script. Scripts can be launched directly from a PeerTube server, or from a separate server, even a desktop PC. You need to follow all the following steps even if you are on a PeerTube server.

You need the development dependencies and access to a PeerTube instance (either local or remote). Go to the directory of that instance to execute the following commands.

```sh
yarn install
npm run build:server
```

## Wrapper <a class="toc" id="toc-wrapper" href="#toc-wrapper"></a>

The wrapper provides a convenient interface to most scripts, and requires the [same dependencies](#dependencies). You can access it as `peertube` via an alias in your `.bashrc` like `alias peertube="node ${PEERTUBE_PATH}/dist/server/tools/peertube.js"`:

```
  Usage: peertube [command] [options]

  Options:

    -v, --version         output the version number
    -h, --help            output usage information

  Commands:

    auth [action]         register your accounts on remote instances to use them with other commands
    upload|up             upload a video
    import-videos|import  import a video from a streaming platform
    watch|w               watch a video in the terminal ✩°｡⋆
    repl                  initiate a REPL to access internals
    help [cmd]            display help for [cmd]
```

The wrapper can keep track of instances you have an account on. We limit to one account per instance for now.

```bash
$ peertube auth add -u "PEERTUBE_URL" -U "PEERTUBE_USER" --password "PEERTUBE_PASSWORD"
$ peertube auth list
┌──────────────────────────────┬──────────────────────────────┐
│ instance                     │ login                        │
├──────────────────────────────┼──────────────────────────────┤
│ "PEERTUBE_URL"               │ "PEERTUBE_USER"              │
└──────────────────────────────┴──────────────────────────────┘
```

You can now use that account to upload videos without feeding the same parameters again.

```bash
$ peertube up <videoFile>
```

And now that your video is online, you can watch it from the confort of your terminal (use `peertube watch --help` to see the supported players):

```bash
$ peertube watch https://peertube.cpy.re/videos/watch/e8a1af4e-414a-4d58-bfe6-2146eed06d10
```
