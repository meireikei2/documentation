# PeerTube Documentation

This repo contains the source for [docs.joinpeertube.org](https://docs.joinpeertube.org). Please refer to the _self-contained documentation of PeerTube for canonical technical reference_.

## :raised_hands: Contributing

You don't need to be a coder to help! The site structure might seem daunting at first, so we also accept contributions in the form of a simple markdown file that you wish to integrate.

You can also join our community for any question or discuss ideas:

- Chat<a name="contact"></a>:
  - IRC : **[#peertube on chat.freenode.net:6697](https://kiwiirc.com/client/irc.freenode.net/#peertube)**
  - Matrix (bridged on the IRC channel) : **[#peertube:matrix.org](https://matrix.to/#/#peertube:matrix.org)**
- Forum:
  - Framacolibri: [https://framacolibri.org/c/peertube](https://framacolibri.org/c/peertube)

## Site structure

You will probably want to look at the files in `lang/en`, and once you have either edited them or made your own, make sure they appear in the proper guide in `_data`.

```
_config -> Jekyll config file
_data -> main configuration (guides in particular)
└── i18n
_layouts -> page structures
└── pages
_sass -> CSS and styling
_site
lang -> main Markdown folder
└── en -> the source language is english
    ├── devdocs
    ├── docs
    ├── org
    └── userdocs
```

## Building the site

You should ensure you have Yarn and [Bundler](http://bundler.io/) installed:

```sh
$ gem install bundler
```

And then:

```sh
$ make
```

Or:

```sh
$ make install
$ make serve
```

On Windows, `make` is not available, so you need to execute `bundle` and `jekyll` directly:

```sh
bundle install
bundle exec jekyll serve --incremental
```
